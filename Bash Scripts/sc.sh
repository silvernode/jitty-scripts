#!/bin/bash

# SC - Service Commander - frontend for the sv command

###############################################################################################
## Copyright (c) 2015, Justin Moore
##
## Permission to use, copy, modify, and/or distribute this software 
## for any purpose with or without fee is hereby granted, 
## provided that the above copyright notice and this permission notice appear in all copies.
##
## THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
## WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY 
## AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, 
## OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
## WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
## ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
##############################################################################################

case $1 in

        link | --link-service )
            ln -v -s /etc/sv/$2 /var/service/
          ;;

        unlink | --remove-service )
            rm -v /var/service/$2
          ;;
			
				linked | --linked-services )
						cd /etc/sv && find -xtype l -exec ls -l {} \;
					;;

				list | ls | --list )

						echo
						echo -e "\e[92m Contents of /etc/sv/\e[0m"
						#echo "Contents of /etc/sv/"
						echo
						ls /etc/sv
						echo
						echo -e "\e[96m Contents of /var/service/\e[0m"
						echo
						ls /var/service
						echo
					;;

				up )
						sv s /var/service/*
					;;

				start )
						sv start $2
					;;

				restart )
						sv restart $2
					;;

				stop )
						sv stop $2
					;;

        --help | -h | help )
					echo
					echo " sc - Service Commander - frontend for the sv command"
					echo
					echo " Usage: sc link | unlink | linked | list | up | start | restart | stop <service>"
					echo
					echo
					echo " For detailed help, see the commands below:"
					echo
					echo
					echo " help-all"
					echo " help-link"
					echo " help-unlink"
					echo " help-linked"
					echo " help-list"
					echo " help-up"
					echo " help-start"
					echo " help-restart"
					echo " help-stop"
					echo
					;;

				help-link )
					echo
					echo "link : Creates a symlink from /etc/sv/<name> -> /var/service/<name>"
					echo
					;;
				
				help-unlink )
					echo
					echo "unlink : Removes symlink from /etc/sv/<name> -> /var/service/<name>"
					echo
					;;

				help-linked )
					echo
					echo "linked : Lists all symlinked items in /etc/sv"
					echo
					;;

				help-list )
					echo
					echo "list : List the contents of /etc/sv and /var/service"
					echo
					;;

				help-up )
					echo
					echo "up : Lists all currently running services"
					echo
					;;

				help-start )
					echo
					echo "start : Uses the 'sv' command to start a service"
					echo
					;;

				help-restart )
					echo
					echo "restart : Uses the 'sv' command to restart a service"
					echo
					;;

				help-stop )
					echo
					echo "stop : Uses the 'sv' command to stop a service"
					echo
					;;

				help-all )
					echo
					echo "link : Creates a symlink from /etc/sv/<name> -> /var/service/<name>"
					echo "unlink : Removes symlink from /etc/sv/<name> -> /var/service/<name>"
					echo "linked : Lists all symlinked items in /etc/sv"
					echo "list : List the contents of /etc/sv and /var/service"
					echo "up : Lists all currently running services"
					echo "start : Uses the 'sv' command to start a service"
					echo "restart : Uses the 'sv' command to restart a service"
					echo "stop : Uses the 'sv' command to stop a service"
					echo
					;;
				*)
					echo
					echo " sc - Service Commander - frontend for the sv command"
					echo
					echo " Usage: sc link | unlink | linked | list | up | start | restart | stop <service>"
					echo
					echo
					echo " For detailed help, see the commands below:"
					echo
					echo
					echo " help-all"
					echo " help-link"
					echo " help-unlink"
					echo " help-linked"
					echo " help-list"
					echo " help-up"
					echo " help-start"
					echo " help-restart"
					echo " help-stop"
					echo

            ;;
esac

