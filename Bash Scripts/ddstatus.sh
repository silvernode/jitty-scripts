#!/bin/bash


PID=$(pidof dd)


main(){

  while true;do
    clear
    if [ ! ${PID} ];then
      echo "DD is not running"
      exit 0;
    elif [[ ${PID} ]];then
      kill -USR1 ${PID} > /dev/null
      sleep 1
      if [[ ! ${PID} ]];then
        echo "dd it no longer running"
        exit 0;
      fi
    else
      echo "There was an unknown error, exiting..."
      exit 0;
    fi
  done
}

main
