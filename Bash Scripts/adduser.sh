#!/bin/bash



Create_user(){

	echo
	echo -n "Username: "
	read uname

	echo "Additional Groups: "
	echo
	echo "[1] - Arch Linux Defaults"
	echo "[2] - Void Linux Defaults"
	echo "[3] - Custom"
	read dgroups

	if [ "${dgroups}" = "1" ];then
		addgroups="power, wheel, optical network, video, audio, storage"
	elif [ "${dgroups}" = "2" ];then
		addgroups="wheel,storage,audio,video,optical,lp,network,dbus,xbuilder"
	elif [ "${dgroups}" = "3" ];then
		echo -n "Enter desired groups (comma separated): "
		read custgroups
		addgroups="${custgroups}"
	fi
	echo "${addgroups}"
	read -n 1 -p "wait"
	useradd -m -g users -G ${dgroups} -s /bin/bash ${uname}
  passwd
	echo

	if [ -d /home/${uname} ];then
		echo "User ${uname} created"
	else
		echo "User creation failed"
	fi
}

Remove_user(){
	echo
	echo -n "Username to delete: "
	read duser

	userdel ${duser}
	if [ -d /home/${duser} ];then
		rm -R /home/${duser}
	else
		echo "User deletion failed"
	fi
}
main(){

	while true;do

		clear
		echo
		echo "[1] - Add User"
		echo "[2] - Remove User"
		echo
		echo -n "Choose: "
		read choice


		if [ "${choice}" = "1" ];then
			Create_user
		else
			echo "no"
		fi
done
}

case "$1" in
	add ) Create_user;;
	remove ) Remove_user;;
	* ) echo "Usage: mkusr [add | remove]";;
esac
