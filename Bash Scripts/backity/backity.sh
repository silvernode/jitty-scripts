#!/bin/bash


localBackup(){
	echo -n "Directory or file to backup: "
	read input

	echo -n "Output directory: "
	read output

	duplicity -v ${input} file//${output}
}

localBackup
