#!/bin/bash

#=====| General Options |======#
#default directory to backup
DEFAULTDIR="/home/${USER}"
BACKUPNAME="$(date +"%m_%d_%Y")"

#======| Network Options |=====#

PROTOCOL=
SERVERNAME=""
